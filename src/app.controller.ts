import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { loggers } from 'winston';
import { AppService } from './app.service';
import { GeneralResponse } from './dto/api/response/general-response.dto';
import { ValidateEmailResponseDto } from './dto/api/response/validate-email-response.dto';
import { ValidatePhoneResponseDto } from './dto/api/response/validate-phone-response.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @MessagePattern('validatePhone')
  async validatePhone(
    phoneNum: string,
  ): Promise<GeneralResponse<ValidatePhoneResponseDto>> {
    return await this.appService.validatePhone(phoneNum);
  }

  @MessagePattern('validateEmail')
  async validateEmail(
    email: string,
  ): Promise<GeneralResponse<ValidateEmailResponseDto>> {
    return await this.appService.validateEmail(email);
  }

  @MessagePattern('healthz')
  healthz(): GeneralResponse<string> {
    return this.appService.healthz();
  }
}
