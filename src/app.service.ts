import { Inject, Injectable, Logger } from '@nestjs/common';
import { firstValueFrom } from 'rxjs';
import { ValidateEmailResponseDto } from './dto/api/response/validate-email-response.dto';
import { ValidatePhoneResponseDto } from './dto/api/response/validate-phone-response.dto';
import { HttpService } from '@nestjs/axios';
import { GeneralResponse } from './dto/api/response/general-response.dto';
@Injectable()
export class AppService {
  private logger = new Logger('AppService');
  private httpService = new HttpService();

  async validatePhone(
    phoneNum: string,
  ): Promise<GeneralResponse<ValidatePhoneResponseDto>> {
    this.logger.log(`Validating for phone number ${phoneNum}`);
    const url = `${process.env.NUMVERIFY_BASE_URL}/validate`;
    const params = {
      access_key: process.env.NUMVERIFY_KEY,
      number: phoneNum,
    };

    const res = await firstValueFrom(
      this.httpService.get(url, { params: params }),
    );
    this.logger.log(`phone number result: ${JSON.stringify(res.data)}`);
    if (res.data.success !== false) {
      return new GeneralResponse<ValidatePhoneResponseDto>(true, res.data);
    } else {
      return new GeneralResponse<ValidatePhoneResponseDto>(
        false,
        null,
        res.data.error,
      );
    }
  }

  async validateEmail(
    email: string,
  ): Promise<GeneralResponse<ValidateEmailResponseDto>> {
    this.logger.log(`Validating for email ${email}`);
    const url = `${process.env.MAILBOXLAYER_BASE_URL}/check`;
    const params = {
      access_key: process.env.MAILBOXLAYER_KEY,
      email: email,
      smtp: 1,
      format: 1,
    };

    const res = await firstValueFrom(
      this.httpService.get(url, { params: params }),
    );
    this.logger.log(`email result: ${JSON.stringify(res.data)}`);
    if (res.data.success !== false) {
      return new GeneralResponse<ValidateEmailResponseDto>(true, res.data);
    } else {
      return new GeneralResponse<ValidateEmailResponseDto>(
        false,
        null,
        res.data.error,
      );
    }
  }

  healthz(): GeneralResponse<string> {
    this.logger.log('healthz get called');
    return new GeneralResponse(true);
  }
}
