import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { WINSTON_MODULE_NEST_PROVIDER } from 'nest-winston';
import { AppModule } from './app.module';

const logger = new Logger('Main');

async function bootstrap() {
  const microserviceOptions = {
    transport: Transport.TCP,
    options: {
      host: process.env.HOST,
      port: process.env.PORT,
    },
  };

  const app = await NestFactory.createMicroservice(
    AppModule,
    microserviceOptions,
  );
  app.useLogger(app.get(WINSTON_MODULE_NEST_PROVIDER));
  await app.listen();
  logger.log(
    `contact-validator-service is listening port ${process.env.PORT}...`,
  );
}
bootstrap();
