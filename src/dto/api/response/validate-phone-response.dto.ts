import { IsBoolean, IsString } from 'class-validator';

export class ValidatePhoneResponseDto {
  @IsBoolean()
  valid: boolean;

  @IsString()
  number: string;

  @IsString()
  local_format: string;

  @IsString()
  international_format: string;

  @IsString()
  country_prefix: string;

  @IsString()
  country_code: string;

  @IsString()
  country_name: string;

  @IsString()
  location: string;

  @IsString()
  carrier: string;

  @IsString()
  line_type: string;
}
