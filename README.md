# Contact Validator Microservice
This is a NestJS microservice for phone number and email validation.

## System Architecture
![System Architecture](ContactValidatorArchitecture.png)

## Prerequisite
This application have to be run together with [contact-validator-frontend](https://bitbucket.org/ericc0504/contact-validator-frontend/src/master/) and [contact-validator-api](https://bitbucket.org/ericc0504/contact-validator-api/src/master/).
Please first check it out.

## Third Party Services
You may first prepare an account and get the api key.

- [numverify](https://numverify.com)
- [mailboxlayer](https://mailboxlayer.com)

## Logging
Logging will be avaialbe in ./log/debug.log

## Getting Started
```bash
# Clone the repo
$ git clone git@bitbucket.org:ericc0504/contact-validator-service.git

# Fill in api keys from the previous step in .env file

# Install dependencies
$ yarn

# Remember to run contact-validator-api first

# Run
$ docker-compose up
```